﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebFormses.Logic;
using WebFormses.Models;
using Microsoft.AspNet.Identity;
using System.Net;

namespace WebFormses
{
    public partial class SiteMaster : MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void SignOut(object sender, EventArgs e)
        {
            var authenticationManager = HttpContext.Current.GetOwinContext().Authentication;
            authenticationManager.SignOut();
            Response.Redirect("~/");
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            var isLoggedIn = HttpContext.Current.User.Identity.IsAuthenticated;
            LogoutButtonPlaceHolder.Visible = isLoggedIn;
            LoginPlaceHolder.Visible = !isLoggedIn;
            RegisterPlaceHolder.Visible = !isLoggedIn;

            using (ShoppingCartActions usersShoppingCart = new ShoppingCartActions())
            {
                string cartStr = string.Format("Cart ({0})", usersShoppingCart.GetCount(ShoppingCartActions.CartId));
                cartCount.InnerText = cartStr;
            }
        }
        public IQueryable<Category> GetCategories()
        {
            var _db = new ProductContext();
            IQueryable<Category> query = _db.Categories;
            return query;
        }

    }
}